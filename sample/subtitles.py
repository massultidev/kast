#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
from pathlib import Path

from kast.media.processing.MediaInfoExtractor import MediaInfoExtractor
from kast.media.processing.SubtitleUtils import SubtitleUtils


class MultiTypeValidator:
    def __init__(self, types):
        self._types = types
        self._count = 0

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}(types=({", ".join([cls.__name__ for cls in self._types])}))'

    def __call__(self, value):
        if self._count >= len(self._types):
            raise ValueError(f'Numer of expected args exceeded! Actual/Expected: {self._count + 1}/{len(self._types)}')

        try:
            return self._types[self._count](value)

        finally:
            self._count += 1


def parseArgs():
    parser = argparse.ArgumentParser()
    mutuallyExclusiveOptions = parser.add_mutually_exclusive_group(required=True)
    mutuallyExclusiveOptions.add_argument('-l', '--list', action='store_true', help='list subtitle streams in container')
    mutuallyExclusiveOptions.add_argument('-e', '--extract', nargs=2, metavar=('<stream-id>', '<output-path>'),
        type=MultiTypeValidator(types=(int, str)), help='extract subtitles by <stream-id> to <output-path>')
    parser.add_argument('media', help='choose media container file')
    return parser.parse_args()


def actionList(parsedArgs):
    metaData = MediaInfoExtractor.extractMetaData(inputFile=Path(parsedArgs.media))
    print('Subtitle streams:')
    print('---')
    for subtitleStreamInfo in metaData.subtitleStreams:
        print(subtitleStreamInfo)


def actionExtract(parsedArgs):
    print('Extracting subtitles:')
    print('---')
    inputFileStr, streamId, outputFileStr = [parsedArgs.media] + list(parsedArgs.extract)
    metaData = MediaInfoExtractor.extractMetaData(inputFile=Path(inputFileStr))
    print(f'Input: \'{Path(inputFileStr).absolute()}\'')
    print('Stream:', metaData.subtitleStreams[streamId])
    print(f'Output: \'{Path(outputFileStr).absolute()}\'')
    SubtitleUtils.extract(
        inputFile=Path(inputFileStr),
        streamId=streamId,
        outputFile=Path(outputFileStr)
    )


def main():
    parsedArgs = parseArgs()

    print(f'Parsed Args: {parsedArgs}\n')

    if parsedArgs.list:
        actionList(parsedArgs=parsedArgs)
        return

    actionExtract(parsedArgs=parsedArgs)


if __name__ == '__main__':
    main()
