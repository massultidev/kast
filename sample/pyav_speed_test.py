#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
import logging
import os
import time
from collections.abc import Callable
from dataclasses import dataclass
from enum import Enum
from pathlib import Path

import av
from av import AudioFrame, AudioStream, VideoFrame, VideoStream
from tunit.unit import Seconds

from kast.utils.StopWatch import StopWatch
from kast.utils.log.Loggable import Loggable
from kast.utils.log.NonBlockingLogHandler import NonBlockingLogHandler

type AvStream = AudioStream | VideoStream
type AvFrame = AudioFrame | VideoFrame


class EnumBase(Enum):

    @classmethod
    def values(cls) -> list[str]:
        return [pf.value for pf in cls]


class PixelFormat(EnumBase):
    NONE = '-'
    RGB_24 = 'rgb24'
    RBG_32 = 'rgb32'
    YUV_420P = 'yuv420p'
    NV12 = 'nv12'


class ThreadType(EnumBase):
    DEFAULT = '-'
    AUTO = 'AUTO'
    FRAME = 'FRAME'
    SLICE = 'SLICE'


@dataclass
class ArgsProtocol:
    media: str
    pixelFormat: PixelFormat
    videoThreadType: ThreadType
    audioThreadType: ThreadType
    sleepSeconds: float


type FpsUpdateCallback = Callable[[int], None]


class FpsCounter:

    def __init__(self, onFpsUpdate: FpsUpdateCallback) -> None:
        super().__init__()
        self._stopWatch = StopWatch()
        self._fpsCounter: int = 0
        self._fpsCurrent: int = 0
        self._onFpsUpdate: FpsUpdateCallback = onFpsUpdate

    @property
    def fps(self) -> int:
        return self._fpsCurrent

    def reset(self) -> None:
        self._reset()

    def tick(self) -> None:
        if self._stopWatch.elapsed >= Seconds(1):
            self._reset(value=self._fpsCounter)
            self._onFpsUpdate(self._fpsCurrent)

        self._stopWatch.start()
        self._fpsCounter += 1

    def _reset(self, value: int = 0) -> None:
        self._stopWatch.stop()
        self._fpsCounter = 0
        self._fpsCurrent = value


class AvSpeedTest(Loggable):

    def __init__(self, args: ArgsProtocol) -> None:
        self._args: ArgsProtocol = args

        self._fpsCounterVideo: FpsCounter = FpsCounter(onFpsUpdate=lambda fps: self._onFpsUpdate())
        self._fpsCounterAudio: FpsCounter = FpsCounter(onFpsUpdate=lambda fps: self._onFpsUpdate())

    def run(self) -> None:
        with av.open(self._args.media) as container:
            self.log.info(f'Container info:\n')
            self.log.info(f'---\n')
            self.log.info(f'file="{self._args.media}"\n')
            containerType = Path(self._args.media).suffix.split('.', maxsplit=1)[-1]
            self.log.info(f'container={containerType}\n')
            duration = float(container.duration / av.time_base)
            self.log.info(f'{duration=}[s]\n')
            self.log.info('\n')

            self.log.info('Video stream info:\n')
            self.log.info(f'---\n')
            videoStream = container.streams.video[0]
            self._setThreadType(stream=videoStream, threadType=self._args.videoThreadType)
            videoCodecContext = videoStream.codec_context
            self.log.info(f'codec={videoCodecContext.name}\n')
            self.log.info(f'resolution={videoCodecContext.width}x{videoCodecContext.height}\n')
            self.log.info(f'frameRate={int(videoStream.guessed_rate)}\n')
            self.log.info('\n')

            self.log.info('Audio stream info:\n')
            self.log.info(f'---\n')
            audioStream = container.streams.audio[0]
            self._setThreadType(stream=audioStream, threadType=self._args.audioThreadType)
            audioCodecContext = audioStream.codec_context
            self.log.info(f'codec={audioCodecContext.name}\n')
            self.log.info(f'sampleRate={audioCodecContext.sample_rate}\n')
            self.log.info(f'sampleBits={audioCodecContext.format.bits}\n')
            self.log.info(f'sampleBytes={audioCodecContext.format.bytes}\n')
            self.log.info(f'channelCount={len(audioCodecContext.layout.channels)}\n')
            self.log.info('\n')

            self.log.info('Benchmark:\n')
            self.log.info(f'---\n')
            pixelFormat = self._args.pixelFormat.value
            self.log.info(f'pixelFormat={pixelFormat}\n')
            self.log.info(f'videoThreadType={videoStream.thread_type}\n')
            self.log.info(f'audioThreadType={audioStream.thread_type}\n')
            self.log.info('\n')

            for frame in container.decode([videoStream, audioStream]):
                self._readFrameData(frame=frame)

                sleepSeconds = self._args.sleepSeconds
                if sleepSeconds > 0.0:
                    time.sleep(sleepSeconds)

    def _readFrameData(self, frame: AvFrame) -> None:
        if isinstance(frame, av.VideoFrame):
            self._readVideoFrame(frame=frame)
            self._fpsCounterVideo.tick()
        else:
            self._readAudioFrame(frame=frame)
            self._fpsCounterAudio.tick()

    def _readVideoFrame(self, frame: av.VideoFrame) -> None:
        pixelFormat = self._args.pixelFormat
        if pixelFormat != PixelFormat.NONE:
            _ = frame.to_ndarray(format=pixelFormat.value)

    def _readAudioFrame(self, frame: av.AudioFrame) -> None:
        _ = frame.to_ndarray().transpose()

    def _onFpsUpdate(self) -> None:
        fpsVideo = self._fpsCounterVideo.fps
        fpsAudio = self._fpsCounterAudio.fps
        fpsSum = fpsVideo + fpsAudio

        self.log.info('\r' + ''.join((os.get_terminal_size().columns - 3) * [' ']))
        self.log.info(f'\rFPS: video={fpsVideo}, audio={fpsAudio}, sum={fpsSum}')

    def _setThreadType(
        self,
        stream: AvStream,
        threadType: ThreadType
    ) -> None:
        if threadType is not ThreadType.DEFAULT:
            stream.thread_type = threadType.value


def setupLogging():
    logFormatter = logging.Formatter(fmt='%(message)s')
    logFormatter.converter = time.gmtime

    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    consoleHandler = logging.StreamHandler()
    consoleHandler.terminator = ''
    consoleHandler.setFormatter(logFormatter)

    nonBlockingHandler = NonBlockingLogHandler(consoleHandler)

    rootLogger.addHandler(nonBlockingHandler)

    av.logging.set_level(av.logging.DEBUG)
    av.logging.restore_default_callback()


def parseArgs() -> ArgsProtocol:
    parser = argparse.ArgumentParser()
    parser.add_argument('media', help='choose media container file')
    parser.add_argument('-pf', '--pixel-format', help='choose pixel format', choices=PixelFormat.values(), default=PixelFormat.RGB_24.value)
    parser.add_argument('-vtt', '--video-thread-type', help='choose video thread type', choices=ThreadType.values(), default=ThreadType.DEFAULT.value)
    parser.add_argument('-att', '--audio-thread-type', help='choose audio thread type', choices=ThreadType.values(), default=ThreadType.DEFAULT.value)
    parser.add_argument('-s', '--sleep', help='specify cooldown sleep interval', type=float, default=0.0)
    parsedArgs = parser.parse_args()
    return ArgsProtocol(
        media=getattr(parsedArgs, 'media'),
        pixelFormat=PixelFormat(getattr(parsedArgs, 'pixel_format')),
        videoThreadType=ThreadType(getattr(parsedArgs, 'video_thread_type')),
        audioThreadType=ThreadType(getattr(parsedArgs, 'audio_thread_type')),
        sleepSeconds=getattr(parsedArgs, 'sleep')
    )


def main() -> None:
    args = parseArgs()
    setupLogging()
    AvSpeedTest(args=args).run()


if __name__ == '__main__':
    main()
