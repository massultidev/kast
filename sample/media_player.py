#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

"""
This is a simple sample script created for testing media player implementations.
"""

import argparse
import logging
import sys
import time
from pathlib import Path

import PyQt5.Qt
import setproctitle
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCloseEvent, QIcon
from PyQt5.QtWidgets import QAction, QActionGroup, QApplication, QFileDialog, QHBoxLayout, QMainWindow, QMenu, \
    QPushButton, \
    QSlider, QStyle, \
    QVBoxLayout, \
    QWidget
from tunit.unit import Milliseconds

from kast import __package__ as PACKAGE_NAME
from kast.core.settings.SettingsKeys import defaultBrowseMediaPath
from kast.interface.qt5.utils.QtHelper import QtHelper
from kast.interface.qt5.utils.mediaPlayer.MediaPlayerFacade import MediaPlayerFacade
from kast.interface.qt5.utils.mediaPlayer.MediaPlayerFactory import MediaPlayerBackend, MediaPlayerFactory
from kast.interface.qt5.utils.mediaPlayer.MediaPlayerState import MediaPlayerState
from kast.interface.qt5.utils.mediaPlayer.frontend.VideoWidget import VideoWidget
from kast.interface.qt5.utils.mediaPlayer.frontend.subtitle.SubtitleStyle import SubtitleStyle
from kast.utils.OsInfo import OsInfo, OsName
from kast.utils.log.Loggable import Loggable
from kast.utils.log.NonBlockingLogHandler import NonBlockingLogHandler

DIR_ROOT = Path(__file__).resolve().parent.parent
DIR_ASSETS = DIR_ROOT / 'pkg' / PACKAGE_NAME / 'assets'
FILE_LOGO = DIR_ASSETS / 'kast.png'


class VideoWindow(Loggable, QMainWindow):

    def __init__(self, backend, parent=None):
        super(VideoWindow, self).__init__(parent)
        title = "MediaPlayer"
        self.setWindowTitle(title)
        setproctitle.setproctitle(title)
        QtHelper.getApp().setApplicationName(title)

        self._lastMediaDir: Path = defaultBrowseMediaPath()

        self.log.info(f"PyQt: {PyQt5.Qt.PYQT_VERSION_STR}, Qt: {PyQt5.Qt.QT_VERSION_STR}")

        self.videoWidget = videoWidget = VideoWidget(
            logoPath=FILE_LOGO,
            parent=self
        )
        videoWidget.displayFps = True

        MediaPlayerFactory._IGNORE_PLATFORM_RESTRICTIONS = True

        backendEnum = MediaPlayerBackend(backend)
        self.mediaPlayer: MediaPlayerFacade = MediaPlayerFacade(
            backend=backendEnum,
            surface=videoWidget.surface,
            parent=self
        )
        self.mediaPlayer.init()

        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setIcon(self._getPlayButtonIcon())
        self.playButton.clicked.connect(self.play)

        self.stopButton = QPushButton()
        self.stopButton.setEnabled(False)
        self.stopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.stopButton.clicked.connect(self.mediaPlayer.stop)

        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setTracking(False)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderReleased.connect(self.setPosition)

        self.buttonMute = QPushButton()
        self.buttonMute.setIcon(self._getMuteButtonIcon())
        self.buttonMute.clicked.connect(self.setVolumeMuted)

        self.volumeSlider = QSlider()
        self.volumeSlider.setTracking(False)
        self.volumeSlider.setRange(0, 100)  # Qt backend does not support volume level over 100!
        self.volumeSlider.setValue(int(self.mediaPlayer.volumeLevel * 100))
        self.volumeSlider.setOrientation(Qt.Horizontal)
        self.volumeSlider.setTickPosition(QSlider.TicksAbove)
        self.volumeSlider.setTickInterval(10)
        self.volumeSlider.sliderReleased.connect(self.setVolumeLevel)

        openAction = QAction(QIcon('open.png'), '&Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open movie')
        openAction.triggered.connect(self.openFileMenuAction)

        openSubtitles = QAction(QIcon('open.png'), '&Subtitles', self)
        openSubtitles.setShortcut('Ctrl+S')
        openSubtitles.setStatusTip('Open subtitles')
        openSubtitles.triggered.connect(self.openSubtitlesMenuAction)

        exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.closeMenuAction)

        def onBackendChange(backendEnum):
            return lambda: self.onBackendChange(backendEnum)

        backendMenu = QMenu('Backend', self)
        backendEngineMenu = QMenu('Engine', self)
        backendMenu.addMenu(backendEngineMenu)
        backendEngineActionGroup = QActionGroup(self)
        for backendEnum in MediaPlayerBackend:
            backendEngineAction = QAction(backendEnum.name, self)
            backendEngineAction.setCheckable(True)
            backendEngineAction.triggered.connect(onBackendChange(backendEnum))
            backendEngineMenu.addAction(backendEngineActionGroup.addAction(backendEngineAction))
            if backendEnum.value == backend:
                backendEngineAction.setChecked(True)

        def onSubtitleStyleChange(subtitleStyle):
            return lambda: self.onSubtitleStyleChange(subtitleStyle)

        frontendMenu = QMenu('Frontend', self)
        frontendSubtitleStyleMenu = QMenu('Subtitle style', self)
        frontendMenu.addMenu(frontendSubtitleStyleMenu)
        frontendSubtitleStyleActionGroup = QActionGroup(self)
        for subtitleStyle in SubtitleStyle:
            subtitleStyleAction = QAction(subtitleStyle.name, self)
            subtitleStyleAction.setCheckable(True)
            subtitleStyleAction.triggered.connect(onSubtitleStyleChange(subtitleStyle))
            frontendSubtitleStyleMenu.addAction(frontendSubtitleStyleActionGroup.addAction(subtitleStyleAction))
            if subtitleStyle == self.videoWidget.surface.subtitleStyle:
                subtitleStyleAction.setChecked(True)


        menuBar = self.menuBar()
        fileMenu = menuBar.addMenu('&File')
        fileMenu.addAction(openAction)
        fileMenu.addAction(openSubtitles)
        fileMenu.addAction(exitAction)
        menuBar.addMenu(backendMenu)
        menuBar.addMenu(frontendMenu)

        wid = QWidget(self)
        self.setCentralWidget(wid)

        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.stopButton)
        controlLayout.addWidget(self.positionSlider, 1)
        controlLayout.addWidget(self.buttonMute)
        controlLayout.addWidget(self.volumeSlider)

        layout = QVBoxLayout()
        layout.addWidget(videoWidget)
        layout.addLayout(controlLayout)

        wid.setLayout(layout)

        self.mediaPlayer.signals.signalOnStateChange.connect(self.onStateChanged)
        self.mediaPlayer.signals.signalOnDurationChange.connect(self.onDurationChanged)
        self.mediaPlayer.signals.signalOnPositionChange.connect(self.onPositionChanged)
        self.mediaPlayer.signals.signalOnVolumeMutedChange.connect(self.onVolumeMutedChanged)
        self.mediaPlayer.signals.signalOnVolumeLevelChange.connect(self.onVolumeLevelChanged)

    def closeEvent(self, event: QCloseEvent) -> None:
        self.mediaPlayer.shutdown()
        super().closeEvent(event)

    def closeMenuAction(self):
        self.close()

    def openFileMenuAction(self):
        fileName, _ = QFileDialog.getOpenFileName(self, "Open Movie", str(self._lastMediaDir))

        if fileName != '':
            filePath = Path(fileName)
            self._lastMediaDir = filePath.parent
            self.mediaPlayer.setMediaFile(filePath)
            self.playButton.setEnabled(True)
            self.stopButton.setEnabled(True)

    def openSubtitlesMenuAction(self):
        fileName, _ = QFileDialog.getOpenFileName(self, "Open Subtitles", str(self._lastMediaDir))

        if fileName != '':
            filePath = Path(fileName)
            self._lastMediaDir = filePath.parent
            self.mediaPlayer.setSubtitleFile(filePath)

    def play(self):
        if self.mediaPlayer.state == MediaPlayerState.Playing:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def onBackendChange(self, backend):
        self.mediaPlayer.setBackend(backend)

    def onSubtitleStyleChange(self, subtitleStyle):
        self.videoWidget.surface.setSubtitleStyle(style=subtitleStyle)

    def onStateChanged(self, state):
        self.playButton.setIcon(self._getPlayButtonIcon())

    def onVolumeMutedChanged(self, muted):
        self.buttonMute.setIcon(self._getMuteButtonIcon())

    def onVolumeLevelChanged(self, level):
        if not self.volumeSlider.isSliderDown():
            self.volumeSlider.setValue(int(level * 100))

    def onPositionChanged(self, position):
        if not self.positionSlider.isSliderDown():
            self.positionSlider.setValue(position)

    def onDurationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self):
        position = self.positionSlider.sliderPosition()
        self.mediaPlayer.seek(position=Milliseconds(position))

    def setVolumeMuted(self):
        self.mediaPlayer.volumeMuted = (not self.mediaPlayer.volumeMuted)

    def setVolumeLevel(self):
        volumeLevel = self.volumeSlider.sliderPosition() / 100
        self.mediaPlayer.volumeLevel = volumeLevel

    def _getMuteButtonIcon(self):
        iconId = QStyle.SP_MediaVolumeMuted if self.mediaPlayer.volumeMuted \
            else QStyle.SP_MediaVolume
        return self.style().standardIcon(iconId)

    def _getPlayButtonIcon(self):
        iconId = QStyle.SP_MediaPause if self.mediaPlayer.state is MediaPlayerState.Playing \
            else QStyle.SP_MediaPlay
        return self.style().standardIcon(iconId)


BACKENDS = [backend.value for backend in MediaPlayerBackend]
DEFAULT_BACKEND = MediaPlayerBackend.PyAV.value


def initDebugCapabilities() -> None:
    if OsInfo.name == OsName.Linux:
        try:
            import namedthreads
            namedthreads.patch()
        except Exception as ex:
            logging.exception('Exporting thread names to OS failed!', ex)


def setupExternalLoggers():
    logging.getLogger('asyncio').setLevel(logging.INFO)


def setupLogging():
    logFormatter = logging.Formatter(
        fmt='[%(asctime)s][%(levelname)s][Thread:%(threadName)s][%(name)s]: %(message)s',
        datefmt='%Y.%m.%dT%H:%M:%S%z'
    )
    logFormatter.converter = time.gmtime

    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)

    nonBlockingHandler = NonBlockingLogHandler(consoleHandler)

    rootLogger.addHandler(nonBlockingHandler)

    setupExternalLoggers()


def main():
    setupLogging()
    initDebugCapabilities()

    parser = argparse.ArgumentParser()
    parser.add_argument('backend', help='choose backend', choices=BACKENDS, nargs='?', default=DEFAULT_BACKEND)
    parsedArgs = parser.parse_args()

    app = QApplication(sys.argv)
    player = VideoWindow(backend=parsedArgs.backend)
    player.resize(640, 480)
    player.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
