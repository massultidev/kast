#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import pytest

from kast.media.processing.Resolution import FULL_HD, Resolution, ULTRA_HD


@pytest.mark.parametrize(
    'boundingResolution, initialResolution, expectedResolution',
    [
        (  # Greater than bounds, same proportions. -> Result == Bounds
            FULL_HD,
            ULTRA_HD,
            FULL_HD,
        ),
        (  # Lesser than bounds. -> Result == Initial
            Resolution(width=1920, height=1080),
            Resolution(width=1024, height=500),
            Resolution(width=1024, height=500),
        ),
        (   # Greater width. -> Width == Bound && Height == Scaled
            Resolution(width=1920, height=1080),
            Resolution(width=2048, height=1024),
            Resolution(width=1920, height=960),
        ),
        (  # Greater height. -> Width == Scaled && Height == Bound
            Resolution(width=1920, height=1080),
            Resolution(width=1920, height=1280),
            Resolution(width=1620, height=1080),
        ),
    ]
)
def test_shrinkToBounds(
    boundingResolution: Resolution,
    initialResolution: Resolution,
    expectedResolution: Resolution
) -> None:
    assert initialResolution.shrinkToFit(boundingResolution) == expectedResolution


@pytest.mark.parametrize(
    'resolution, expectedStr',
    [
        (FULL_HD, '1920x1080'),
        (ULTRA_HD, '3840x2160'),
        (Resolution(width=800, height=600), '800x600'),
    ]
)
def test_shouldDeserializeFromString(
    resolution: Resolution,
    expectedStr: str
) -> None:
    assert str(resolution) == expectedStr


@pytest.mark.parametrize(
    'resolutionStr, expectedResolution',
    [
        ('1920x1080', FULL_HD),
        ('3840x2160', ULTRA_HD),
        ('800x600', Resolution(width=800, height=600)),
    ]
)
def test_shouldDeserializeFromString(
    resolutionStr: str,
    expectedResolution: Resolution
) -> None:
    assert Resolution.fromStr(resolutionStr=resolutionStr) == expectedResolution
