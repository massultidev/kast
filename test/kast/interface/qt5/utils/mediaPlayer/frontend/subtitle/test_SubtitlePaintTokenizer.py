#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import inspect

import pytest
from PyQt5.QtGui import QFont, QFontMetrics
from pytestqt.qtbot import QtBot

from kast.interface.qt5.utils.mediaPlayer.frontend.subtitle.SubtitlePaintTokenizer import SubtitlePaintTokenizer


@pytest.mark.parametrize(
    'fontPointSize, maxWidthInChar, text, expectedTokens',
    [
        (
            20,
            32,
            'Simple one-line subtitle text.',
            [
                'Simple one-line subtitle text.',
            ]
        ),
        (
            20,
            32,
            'Simple one-line subtitle text, but much longer this time around.',
            [
                'Simple one-line subtitle text, but much',
                'longer this time around.',
            ]
        ),
        (
            20,
            32,
            'Simple multi-line subtitle text.\nNot a very long one.',
            [
                'Simple multi-line subtitle text.',
                'Not a very long one.',
            ]
        ),
        (
            20,
            32,
            inspect.cleandoc('''
                Some random test text.
                Very long line for verification, and now I'm just going to blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah
            
                 Line with extra whitespace at the beginning.
                Line with extra whitespace at the end. 
            
                Line with   multiple     white space in the middle.
                Again some line with multiple     white    space in the middle.
            
                Very-long-line-without-white-space-which-I'll-fill-with-nonsense-to-achieve-the-effect-blahblahblahblahblahblahblahblahblahblahblahblahblahblahblah
            
                Another line after line split.
                In the middle, line with ellipsis...
                Last line!
                '''),
            [
                'Some random test text.',
                'Very long line for verification, and now',
                "I'm just going to blah, blah, blah, blah,",
                'blah, blah, blah, blah, blah, blah, blah,',
                'blah, blah, blah, blah, blah',
                '',
                ' Line with extra whitespace at the',
                'beginning.',
                'Line with extra whitespace at the end. ',
                '',
                'Line with   multiple     white space in the',
                'middle.',
                'Again some line with multiple     white   ',
                'space in the middle.',
                '',
                'Very-long-line-without-white-space-whic-',
                "h-I'll-fill-with-nonsense-to-achieve-the-e-",
                'ffect-blahblahblahblahblahblahblahblah-',
                'blahblahblahblahblahblahblah',
                '',
                'Another line after line split.',
                'In the middle, line with ellipsis...',
                'Last line!',
            ]
        ),
    ]
)
def test_tokenize(
    fontPointSize: int,
    maxWidthInChar: int,
    text: str,
    expectedTokens: list[str],
    qtbot: QtBot  # Important! Enables pytest Qt support!
) -> None:
    __ = qtbot

    font = QFont()
    font.setPointSize(fontPointSize)
    fontMetrics = QFontMetrics(font)

    maxWidthInPixel = maxWidthInChar * fontMetrics.averageCharWidth()

    result = list(SubtitlePaintTokenizer(font=font).tokenize(text=text, maxPixelWidth=maxWidthInPixel))

    assert result == expectedTokens
