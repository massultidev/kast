#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import Any

import pytest
from pytest_mock import MockerFixture

from kast.utils.functional.Maybe import Maybe


@pytest.mark.parametrize(
    'value',
    [
        None,
        7,
        'seven',
        [7],
        {'seven': 7},
    ]
)
def test_shouldAccessStoredValue(value: Any) -> None:
    assert Maybe(value=value).value == value


def test_shouldAssignPropertyOnlyIfMaybePresent(mocker: MockerFixture) -> None:
    assignedValue: int = 1

    class TypeWithProp:
        intProp: int = 0

    intPropMock = mocker.PropertyMock()
    mocker.patch.multiple(
        TypeWithProp,
        intProp=intPropMock
    )

    maybe: Maybe[TypeWithProp] = Maybe(TypeWithProp())
    maybe.asPropertyAssigner().intProp = assignedValue

    maybe: Maybe[TypeWithProp] = Maybe()
    maybe.asPropertyAssigner().intProp = assignedValue

    intPropMock.assert_called_once_with(assignedValue)


def test_shouldProperlyReportContentState() -> None:
    maybe: Maybe[int] = Maybe()

    assert maybe.isEmpty()
    assert not maybe.isPresent()

    maybe = Maybe(1)

    assert not maybe.isEmpty()
    assert maybe.isPresent()


def test_shouldExecuteIfEmpty(mocker: MockerFixture) -> None:
    callback = mocker.Mock()

    maybe: Maybe[int] = Maybe(1)
    maybe.ifEmpty(callback)

    maybe = Maybe()
    maybe.ifEmpty(callback)

    callback.assert_called_once_with()


def test_shouldExecuteIfPresent(mocker: MockerFixture) -> None:
    value: int = 7

    callback = mocker.Mock()

    maybe: Maybe[int] = Maybe(value)
    maybe.ifPresent(callback)

    maybe = Maybe()
    maybe.ifPresent(callback)

    callback.assert_called_once_with(value)


def test_shouldExecutePresentCallback(mocker: MockerFixture) -> None:
    value: int = 7

    callbackEmpty = mocker.Mock()
    callbackPresent = mocker.Mock()

    maybe: Maybe[int] = Maybe(value)
    maybe.ifPresentOrEmpty(
        callbackPresent,
        callbackEmpty
    )

    callbackEmpty.assert_not_called()
    callbackPresent.assert_called_once_with(value)


def test_shouldExecuteEmptyCallback(mocker: MockerFixture) -> None:
    callbackEmpty = mocker.Mock()
    callbackPresent = mocker.Mock()

    maybe: Maybe[int] = Maybe()
    maybe.ifPresentOrEmpty(
        callbackPresent,
        callbackEmpty
    )

    callbackEmpty.assert_called_once_with()
    callbackPresent.assert_not_called()


def test_shouldSwitchToOtherMaybeOnEmpty() -> None:
    wrappedValue = 7

    maybe1: Maybe[int] = Maybe()
    maybe2: Maybe[int] = Maybe(wrappedValue)

    resultMaybe = maybe1.orMaybe(maybe2)

    assert resultMaybe.value == wrappedValue


def test_shouldNotSwitchToOtherMaybeOnPresent() -> None:
    wrappedValue1 = 21
    wrappedValue2 = 7

    maybe1: Maybe[int] = Maybe(wrappedValue1)
    maybe2: Maybe[int] = Maybe(wrappedValue2)

    resultMaybe = maybe1.orMaybe(maybe2)

    assert resultMaybe.value == wrappedValue1


def test_shouldReturnElseIfEmpty() -> None:
    elseValue = 7
    assert Maybe().orElse(elseValue) == elseValue


def test_shouldNotReturnElseIfPresent() -> None:
    wrappedValue = 14
    elseValue = 7
    assert Maybe(wrappedValue).orElse(elseValue) == wrappedValue


def test_shouldGetElseIfEmpty(mocker: MockerFixture) -> None:
    elseValue = 21
    elseProvider = mocker.Mock(return_value=elseValue)
    assert Maybe().orElseGet(elseProvider) == elseValue


def test_shouldNotGetElseIfPresent(mocker: MockerFixture) -> None:
    wrappedValue = 7
    elseValue = 21
    elseProvider = mocker.Mock(return_value=elseValue)
    assert Maybe(wrappedValue).orElseGet(elseProvider) == wrappedValue


def test_shouldThrowIfEmpty() -> None:
    class TestException(Exception):
        pass

    with pytest.raises(TestException):
        Maybe().orThrow(lambda: TestException())


def test_shouldNotThrowIfPresent() -> None:
    wrappedValue = 7
    assert Maybe(wrappedValue).orThrow(lambda: Exception()) == wrappedValue


def test_shouldNotMapIfEmpty(mocker: MockerFixture) -> None:
    mapper = mocker.Mock(return_value=7)
    assert Maybe().map(mapper).isEmpty()
    mapper.assert_not_called()


def test_shouldMapIfPresent(mocker: MockerFixture) -> None:
    mappedValue = 7
    mapper = mocker.Mock(return_value=mappedValue)
    assert Maybe(1).map(mapper).value == mappedValue


def test_shouldNotFlatMapIfEmpty(mocker: MockerFixture) -> None:
    mapper = mocker.Mock(return_value=Maybe(7))
    assert Maybe().flatMap(mapper).isEmpty()
    mapper.assert_not_called()


def test_shouldFlatMapIfPresent(mocker: MockerFixture) -> None:
    mappedValue = 7
    mapper = mocker.Mock(return_value=Maybe(mappedValue))
    assert Maybe(1).flatMap(mapper).value == mappedValue


def test_shouldNotFilterIfEmpty(mocker: MockerFixture) -> None:
    predicate = mocker.Mock(return_value=True)
    assert Maybe().filter(predicate).isEmpty()
    predicate.assert_not_called()


def test_shouldFilterIfPresent(mocker: MockerFixture) -> None:
    wrappedValue = 7
    predicate = mocker.Mock(side_effect=[True, False])
    assert Maybe(wrappedValue).filter(predicate).value == wrappedValue
    assert Maybe(wrappedValue).filter(predicate).isEmpty()
