# Kast distribution scripts

## PyPI release:

- Get your token from PyPI.
- Create file: `security/pypi.token`
- Fill the file with:

```ini
[pypi]
  username = __token__
  password = <your-token-goes-here>
```

- Run: `release_pypi.py`

## Linux release:

### Generic:

- Get your token from BitBucket.
- Create file: `security/bitbucket.token`
- Save your token inside the file as plain text.
- Run: `release_linux.py`

### AUR:

- Get your token from BitBucket.
- Create file: `security/bitbucket.token`
- Save your token inside the file as plain text.
- Run: `release_linux_arch.py`

# Windows release:

- Install `InnoSetup`.
- Make sure dir containing `ISCC.exe` is discoverable in `PATH` environment variable.
- Get your token from BitBucket.
- Create file: `security/bitbucket.token`
- Save your token inside the file as plain text.
- Run: `release_windows.py`
