#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import glob
from dataclasses import dataclass
from pathlib import Path

from core.common import DIR_PACKAGE_ROOT, DIR_ROOT, runCommand


@dataclass
class UicDriverOptions:
    debug: bool = False
    preview: bool = False
    output: str = ''


def main() -> None:
    print(" -- Converting Qt '.ui' files to '.py':")
    for uiFilePath in glob.glob(f'{DIR_PACKAGE_ROOT}/**/*.ui', recursive=True):
        pyFilePath = Path(uiFilePath).with_suffix('.py')

        uiFilePathRelative = Path(uiFilePath).relative_to(DIR_ROOT)
        pyFilePathRelative = Path(pyFilePath).relative_to(DIR_ROOT)

        print(f"'{uiFilePath}' -> '{pyFilePath}'")
        runCommand(*f"-m PyQt5.uic.pyuic -o {pyFilePathRelative} {uiFilePathRelative}".split(' '), cwd=DIR_ROOT)


if __name__ == '__main__':
    main()
