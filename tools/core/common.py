#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import platform
import shutil
import subprocess
import sys
from enum import Enum
from pathlib import Path
from typing import Any

NAME_PACKAGE_ROOT = 'pkg'

DIR_CORE = Path(__file__).resolve().parent
DIR_TOOLS = DIR_CORE.parent
DIR_ROOT = DIR_TOOLS.parent
DIR_PACKAGE_ROOT = DIR_ROOT / NAME_PACKAGE_ROOT
DIR_ARTIFACT = DIR_TOOLS / 'artifact'

sys.path.append(str(DIR_PACKAGE_ROOT))

from kast import __app_url__, __email__, __package__, __version__, __app_name__, __author__, __description__

PACKAGE_NAME = __package__
PACKAGE_VERSION = __version__
PACKAGE_DESCRIPTION = __description__
REPOSITORY_URL = __app_url__
APP_NAME = __app_name__
AUTHOR_NAME = __author__
AUTHOR_EMAIL = __email__


class TargetBase(Enum):
    def __call__(self):
        return str(self)

    def __str__(self):
        return self.name.lower()

    @classmethod
    def all(cls):
        return [eItem() for eItem in cls]

    @classmethod
    def argName(cls):
        return cls.__name__

    @classmethod
    def argHelp(cls):
        return "build target"


def getArchitecture() -> str:
    machine = platform.machine().lower()

    if machine in [
        'x86_64',
        'amd64',
    ]:
        return 'x86_64'

    if machine in [
        'aarch64',
    ]:
        return 'aarch64'

    raise Exception(f"Unsupported architecture '{machine}'!")


def isInPath(cmd: str) -> bool:
    return shutil.which(cmd) is not None


def installProjectPackage() -> None:
    runCommand(*f'-m pip install .'.split(' '), cwd=DIR_ROOT)


def uninstallProjectPackage() -> None:
    runCommand(*f'-m pip uninstall -y {PACKAGE_NAME}'.split(' '))
    runScript(DIR_CORE / 'distclean.py')


def reinstallProjectPackage() -> None:
    uninstallProjectPackage()
    installProjectPackage()


def runScript(scriptFile: Path, *args: Any) -> int:
    return runCommand(*[str(scriptFile), *args], cwd=scriptFile.parent, check=True)


def runCommand(*commandArgs: Any, **runnerArgs: Any) -> int:
    return subprocess.run(['python', *commandArgs], **runnerArgs).returncode
