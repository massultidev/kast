#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
import importlib
import os
import pkgutil
import platform
import shutil
from enum import auto
from typing import Union, get_args

from common import APP_NAME, AUTHOR_NAME, DIR_ARTIFACT, DIR_PACKAGE_ROOT, PACKAGE_NAME, PACKAGE_VERSION, TargetBase, \
    runCommand

DIR_NAME_GST_LIBS = 'system'

DIR_PACKAGE = DIR_PACKAGE_ROOT / PACKAGE_NAME
DIR_ASSETS = DIR_PACKAGE / 'assets'
DIR_BUILD = DIR_ARTIFACT / 'bin'
DIR_DIST = DIR_BUILD / f'{PACKAGE_NAME}.dist'  # It's only for reference! There is no API to change that dir name.


class Target(TargetBase):
    Distclean = auto()
    Clean = auto()
    Bin = auto()
    Dir = auto()


type BuildTarget = Union[Target.Bin, Target.Dir]


def filterArgsNotEmpty(args: list[str]) -> list[str]:
    return [arg for arg in args if arg]


def findAllPackageModules(packageName: str) -> list[str]:
    package = importlib.import_module(packageName)
    return [packageName] + [
        modname
        for importer, modname, isPkg in pkgutil.walk_packages(
            path=package.__path__,
            prefix=package.__name__ + '.',
            onerror=lambda x: None
        )
    ]


class FlagFactory:

    @staticmethod
    def buildTarget(target: BuildTarget) -> str:
        return {
            Target.Dir: '--standalone',
            Target.Bin: '--onefile',
        }.get(target, '')

    @classmethod
    def ignoreModule(cls, modules: list[str]) -> list[str]:
        return cls._makeFlags(flag='nofollow-import-to', values=modules)

    @classmethod
    def pluginEnable(cls, plugins: list[str]) -> list[str]:
        return cls._makeFlags(flag='plugin-enable', values=plugins)

    @classmethod
    def includePackage(cls, packages: list[str]) -> list[str]:
        return cls._makeFlags(flag='include-package', values=packages)

    @classmethod
    def includePackageData(cls, packages: list[str]) -> list[str]:
        return cls._makeFlags(flag='include-package-data', values=packages)

    @classmethod
    def includePackageMetadata(cls, packages: list[str]) -> list[str]:
        return cls._makeFlags(flag='include-distribution-metadata', values=packages)

    @staticmethod
    def _makeFlags(flag: str, values: list[str]) -> list[str]:
        return [f'--{flag}={value}' for value in filterArgsNotEmpty(args=values)]


def _performPostfix() -> None:
    if platform.system() == 'Linux':
        redundantLibs = [
            'libstdc++',
            'libgcc_s',
        ]
        print(" -- Removing redundant libraries: " + ','.join(redundantLibs))
        for redundantLib in redundantLibs:
            os.system(f'rm -fv "{DIR_DIST}"/{redundantLib}*')


def _runCompilation(args: list[str]) -> None:
    cmd = '\n'.join(['python'] + args)
    print(f'-- Executing:\n{cmd}\n')
    if runCommand(*filterArgsNotEmpty(args=args)) != 0:
        raise Exception('Compilation failed!')


def _getPlatformArgs() -> list[str]:
    iconPath = DIR_ASSETS / 'kast.png'

    if platform.system() == 'Linux':
        return [
            f'--linux-icon={iconPath}',
        ]

    if platform.system() == 'Windows':
        return [
            '--low-memory',
            '--jobs=8',
            '--mingw64',
            # '--clang',  # Use together with '--mingw64' to use clang from Mingw64 and not from MSVC.
            '--windows-console-mode=attach', # Set to 'disable' if you want no console at all.
            f'--windows-icon-from-ico={iconPath}',
        ]

    if platform.system() == 'Darwin':
        return [
            '--clang',
            '--macos-create-app-bundle',
            '--macos-app-mode=gui',
            f'--macos-app-name={APP_NAME}',
            f'--macos-app-version={PACKAGE_VERSION}',
            f'--macos-app-icon={iconPath}',
            '--macos-app-protected-resource=NSLocalNetworkUsageDescription:Access to local network',
            '--macos-app-protected-resource=NSNearbyInteractionUsageDescription:Detection of nearby devices',
        ]

    return []


def _getCommonAppInfoArgs() -> list[str]:
    return [
        f'--product-name={APP_NAME}',
        f'--file-version={PACKAGE_VERSION}',
        f'--product-version={PACKAGE_VERSION}',
        f'--copyright=Copyright by {AUTHOR_NAME}',
    ]


def build(target: BuildTarget) -> None:
    args = ([
        '-m',
        'nuitka',
        '--static-libpython=no',
        FlagFactory.buildTarget(target=target),
        '--follow-imports',
        *FlagFactory.ignoreModule(modules=[
            'setuptools',
        ]),
        *FlagFactory.pluginEnable(plugins=[
            'pyqt5',
        ]),
        *FlagFactory.includePackage(packages=[
            PACKAGE_NAME,
            *findAllPackageModules('av'),
        ]),
        *FlagFactory.includePackageData(packages=[pkg for pkg in [
            PACKAGE_NAME,
            'soundcard' if platform.system() == 'Linux' else None,
        ] if pkg is not None]),
        *FlagFactory.includePackageMetadata(packages=[
            'tendo',
        ]),
        f'--output-dir={DIR_BUILD}',
        str(DIR_PACKAGE),
    ]
        + _getPlatformArgs()
        + _getCommonAppInfoArgs()
    )

    _runCompilation(args=args)

    if target == Target.Dir:
        _performPostfix()

    print("Success!")


def cleanCache() -> None:
    print("Clearing caches...")
    runCommand(*'-m nuitka --clean-cache=all'.split(' '))
    print("Success!")


def clean() -> None:
    print("Performing cleanup...")
    if not DIR_BUILD.exists():
        print("Nothing to remove!")
        return

    print(f"Removing: {DIR_BUILD}")
    shutil.rmtree(DIR_BUILD)
    print("Success!")


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(Target.argName(), help=Target.argHelp(), choices=Target.all())
    parsedArgs = parser.parse_args()

    target = Target[getattr(parsedArgs, Target.argName()).capitalize()]
    if target in get_args(BuildTarget):
        build(target)
        return

    if target == Target.Distclean:
        cleanCache()

    clean()


if __name__ == "__main__":
    main()
