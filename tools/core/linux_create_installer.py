#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
import os
import platform
import shutil
import sys
from enum import auto

from common import APP_NAME, DIR_ARTIFACT, DIR_PACKAGE_ROOT, PACKAGE_NAME, PACKAGE_VERSION, TargetBase, getArchitecture, \
    isInPath
from create_bin_native import DIR_DIST as DIR_BIN
from upload_bitbucket import uploadFile

DIR_BUILD = DIR_ARTIFACT / 'installer'
DIR_ASSETS = DIR_PACKAGE_ROOT / PACKAGE_NAME / 'assets'

FMT_ICON = 'png'

FILE_INSTALLER = DIR_BUILD / f'setup_{APP_NAME.lower()}_{PACKAGE_VERSION}_linux_{getArchitecture()}.bash'
FILE_DESKTOP = DIR_ASSETS / f'{PACKAGE_NAME}.desktop'
FILE_ICON = DIR_ASSETS / f'{PACKAGE_NAME}.{FMT_ICON}'

REQUIRED_TOOLS = ['bash', 'cat', 'tar', 'base64', 'chmod']

TAG_APP = 'CONTENT_ARCHIVE_APP'
TAG_DESKTOP = 'CONTENT_FILE_DESKTOP'
TAG_ICON = 'CONTENT_FILE_ICON'

CONTENT_INSTALLER_LOGIC = r'''#!/usr/bin/env bash

set -o errexit -o pipefail -o noclobber -o nounset

DEPENDS=(
    grep
    sed
    mkdir
    base64
    tar
    realpath
    ln
)

DEPENDS_MISSING=()
for DEP in ${DEPENDS[@]} ; do
    if ! command -v "${DEP}" >/dev/null 2>/dev/null ; then
        DEPENDS_MISSING+=("${DEP}")
    fi
done
if [ ${#DEPENDS_MISSING[@]} -gt 0 ] ; then
    echo "[ERROR]: Installation dependencies missing (${#DEPENDS_MISSING[@]}): ${DEPENDS_MISSING[@]}"
    exit 1
fi

PURGE=0
ROOT=
DIRNAME_OPT=opt
DIRNAME_USR=usr

while getopts "hdur:" OPTION
do
    case "${OPTION}" in
        h) # Prints this help message.
            echo "$0: Usage:"
            grep '.) #' "${0}" | grep -v 'grep' | sed 's/\(.\)) #/-\1 /g'
            exit 2
            ;;
        d) # Removes installed files. (Requires the same 'r' and 'u' configuration that was used for installation.)
            PURGE=1
            ;;
        r) # Accepts root directory to unpack to. (Must be absolute to work correctly!)
            ROOT="$(realpath -e "${OPTARG}")"
            ;;
        u) # Switches into user style file tree.
            if [ "${ROOT}" = "" ] ; then ROOT="${HOME}" ; fi
            DIRNAME_OPT=.opt
            DIRNAME_USR=.local
            ;;
    esac
done

NAME_APP="'''f'{APP_NAME}'r'''"
NAME_APP_LOWER="'''f'{APP_NAME.lower()}'r'''"

FMT_ICON="'''f'{FMT_ICON}'r'''"

DIR_OPT="${ROOT}/${DIRNAME_OPT}"
DIR_USR="${ROOT}/${DIRNAME_USR}"
DIR_SHARE="${DIR_USR}/share"
DIR_BIN="${DIR_USR}/bin"
DIR_APPLICATIONS="${DIR_SHARE}/applications"
DIR_PIXMAPS="${DIR_SHARE}/pixmaps"

DIR_APP="${DIR_OPT}/${NAME_APP_LOWER}"
FILE_DESKTOP="${DIR_APPLICATIONS}/${NAME_APP_LOWER}.desktop"
FILE_ICON="${DIR_PIXMAPS}/${NAME_APP_LOWER}.${FMT_ICON}"
FILE_EXE="${DIR_APP}/${NAME_APP_LOWER}.dist/${NAME_APP_LOWER}.bin"
FILE_EXE_LINK="${DIR_BIN}/${NAME_APP_LOWER}"

function uninstall() {
    echo "[--] Uninstalling \"${NAME_APP}\" application:"
    rm -fv \
        "${FILE_EXE_LINK}" \
        "${FILE_ICON}" \
        "${FILE_DESKTOP}" \
    rm -rfv "${DIR_APP}"
}

function install() {
    echo "[--] Installing \"${NAME_APP}\" application:"

    echo " -> Extracting binaries to: ${DIR_APP}"
    mkdir -pv "${DIR_APP}"
    echo "${'''f'{TAG_APP}'r'''}" | base64 -d | tar -xJf - --directory="${DIR_APP}"

    echo " -> Extracting desktop file: ${FILE_DESKTOP}"
    mkdir -pv "$(dirname "${FILE_DESKTOP}")"
    echo "${'''f'{TAG_DESKTOP}'r'''}" | base64 -d > "${FILE_DESKTOP}"

    echo " -> Extracting icon file: ${FILE_ICON}"
    mkdir -pv "$(dirname "${FILE_ICON}")"
    echo "${'''f'{TAG_ICON}'r'''}" | base64 -d > "${FILE_ICON}"

    echo " -> Creating executable link: ${FILE_EXE_LINK}"
    local DIR_LINK="$(dirname "${FILE_EXE_LINK}")"
    mkdir -pv "${DIR_LINK}"
    local LINK_TARGET="$(realpath --relative-to="${DIR_LINK}" "${FILE_EXE}")"
    ln -sv "${LINK_TARGET}" "${FILE_EXE_LINK}"
}

function main() {
    if [ "${PURGE}" = "1" ] ; then
        uninstall
        exit $?
    fi

    install
}

'''

CONTENT_INSTALLER_CLOSURE = '''\
main
'''


def appendText(text: str) -> None:
    with open(FILE_INSTALLER, 'a') as fOutput:
        fOutput.write(text)


def appendContentTag(tag: str, cmdThatPrintsContent: str) -> None:
    appendText(f'{tag}="')
    os.system(f'{cmdThatPrintsContent} | base64 >> "{FILE_INSTALLER}"')
    appendText('"\n\n')


def upload() -> None:
    if not FILE_INSTALLER.exists():
        print(f"Installer file does not exists! File missing: {FILE_INSTALLER}")
        sys.exit(1)

    if not uploadFile(FILE_INSTALLER):
        raise RuntimeError('Uploading installer failed!')


def setup() -> None:
    if platform.system() != 'Linux':
        print("Linux installer can only be generated on Linux!")
        sys.exit(1)

    missingTools = [tool for tool in REQUIRED_TOOLS if not isInPath(tool)]
    if len(missingTools) > 0:
        print(f"Cannot generate installer! Tools missing from path: {', '.join(missingTools)}")

    print(f"Creating installer file: {FILE_INSTALLER}")
    DIR_BUILD.mkdir(parents=True, exist_ok=True)
    with open(FILE_INSTALLER, 'w') as fOutput:
        fOutput.write(CONTENT_INSTALLER_LOGIC)

    print(f"Embedding compressed application directory: {DIR_BIN}")
    appendContentTag(TAG_APP, f'cd "{DIR_BIN.parent}" ; tar -cJf - "./{DIR_BIN.name}"')

    print(f"Embedding desktop file: {FILE_DESKTOP}")
    appendContentTag(TAG_DESKTOP, f'cat "{FILE_DESKTOP}"')

    print(f"Embedding icon file: {FILE_ICON}")
    appendContentTag(TAG_ICON, f'cat "{FILE_ICON}"')

    print("Final touches...")
    appendText(CONTENT_INSTALLER_CLOSURE)
    os.system(f'chmod +x "{FILE_INSTALLER}"')

    print("Finished!")


def clean() -> None:
    print(f"Removing: {DIR_BUILD}")
    if DIR_BUILD.exists():
        shutil.rmtree(DIR_BUILD)


class Target(TargetBase):
    Clean = auto()
    Build = auto()
    Upload = auto()


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(Target.argName(), help=Target.argHelp(), choices=Target.all())
    parsedArgs = parser.parse_args()

    target = Target[getattr(parsedArgs, Target.argName()).capitalize()]
    if target == Target.Upload:
        upload()
        return
    if target == Target.Build:
        setup()
        return

    clean()


if __name__ == '__main__':
    main()
