#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
import os
import shutil
import sys
from enum import auto

from common import APP_NAME, AUTHOR_NAME, DIR_ARTIFACT, DIR_ROOT, PACKAGE_VERSION, REPOSITORY_URL, \
    TargetBase, \
    getArchitecture, isInPath
from create_bin_native import DIR_DIST as DIR_BIN
from upload_bitbucket import uploadFile

DIR_BUILD = DIR_ARTIFACT / 'installer'
DIR_DIST = DIR_BUILD

NAME_INSTALLER = f'setup_{APP_NAME.lower()}_{PACKAGE_VERSION}_windows_{getArchitecture()}'

FILE_SCRIPT_ISS = DIR_BUILD / 'script.iss'
FILE_INSTALLER = DIR_BUILD / f'{NAME_INSTALLER}.exe'

CMD_COMPILER = 'ISCC.exe'

ISS = f'''\
; Inno Setup Script for {APP_NAME} application

#define MyInstallerName "{NAME_INSTALLER}"
#define MyAppName "{APP_NAME}"
#define MyAppVersion "{PACKAGE_VERSION}"
#define MyAppPublisher "{AUTHOR_NAME}"
#define MyAppURL "{REPOSITORY_URL}"
#define MyAppExeName "{APP_NAME}.exe"
#define MyAppRoot "{DIR_ROOT}"
#define MyBinDir "{DIR_BIN}"
#define MyOutputDir "{DIR_DIST}"
''' \
r'''
[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{12E1C761-0F3F-4EA8-AA60-FD1302ADCADE}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
UninstallDisplayIcon={app}\{#MyAppExeName}
DefaultDirName={autopf}\{#MyAppName}
DisableProgramGroupPage=yes
LicenseFile="{#MyAppRoot}\LICENSE.txt"
PrivilegesRequiredOverridesAllowed=dialog
OutputDir="{#MyOutputDir}"
Compression=lzma
SolidCompression=yes
OutputBaseFilename={#MyInstallerName}
WizardStyle=modern

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "{#MyBinDir}\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#MyBinDir}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
'''


def upload() -> None:
    print("Uploading installer...")
    if not FILE_INSTALLER.exists():
        print(f"Installer file does not exists! File missing: {FILE_INSTALLER}")
        sys.exit(1)

    uploadFile(FILE_INSTALLER)


def setup() -> None:
    print("Building installer...")
    DIR_BUILD.mkdir(parents=True, exist_ok=True)
    with open(FILE_SCRIPT_ISS, 'w') as fOutput:
        fOutput.write(ISS)

    if not isInPath(cmd=CMD_COMPILER):
        print(
            f"Inno Setup compiler ({CMD_COMPILER}) not in PATH!\n"
            f"Extend your path or use compilation script manually:\n'{FILE_SCRIPT_ISS}'"
        )
        sys.exit(1)

    os.system(f'{CMD_COMPILER} "{FILE_SCRIPT_ISS}"')


def clean() -> None:
    print(f"Removing: {DIR_BUILD}")
    if DIR_BUILD.exists():
        shutil.rmtree(DIR_BUILD)


class Target(TargetBase):
    Clean = auto()
    Build = auto()
    Upload = auto()


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(Target.argName(), help=Target.argHelp(), choices=Target.all())
    parsedArgs = parser.parse_args()

    target = Target[getattr(parsedArgs, Target.argName()).capitalize()]
    if target == Target.Upload:
        upload()
        return
    if target == Target.Build:
        setup()
        return

    clean()


if __name__ == '__main__':
    main()
