#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
from pathlib import Path

import requests

from common import DIR_TOOLS

URL_BITBUCKET = 'https://api.bitbucket.org/2.0/repositories/massultidev/kast/downloads'


def getAccessToken() -> str:
    tokenFilePath = DIR_TOOLS / 'security' / 'bitbucket.token'
    if not tokenFilePath.exists():
        raise RuntimeError(f"Bitbucket access token file does not exist!")
    with open(tokenFilePath) as f:
        return f.read()


def uploadFile(filePath: Path, url: str = URL_BITBUCKET) -> bool:
    headers = {
        'Authorization': f'Bearer {getAccessToken()}'
    }

    files = {
        'files': (filePath.name, open(filePath.resolve(), 'rb')),
    }

    print(f"Uploading to bitbucket: '{filePath}'")
    response = requests.post(
        url,
        files=files,
        headers=headers
    )

    print(f"Response: [{response.status_code}]: {response.reason}")

    return response.ok


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help="File to upload")
    parsedArgs = parser.parse_args()

    filePath = Path(parsedArgs.file)

    uploadFile(filePath)


if __name__ == '__main__':
    main()
