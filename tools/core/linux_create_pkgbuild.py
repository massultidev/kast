#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
import os
import platform
import subprocess
import sys
from enum import auto

from common import PACKAGE_DESCRIPTION, APP_NAME, REPOSITORY_URL, PACKAGE_VERSION, AUTHOR_NAME, AUTHOR_EMAIL, DIR_TOOLS, TargetBase, \
    isInPath
from linux_create_installer import FILE_INSTALLER

NAME_APP_LOWER = APP_NAME.lower()

DIR_BUILD = DIR_TOOLS / 'pkgbuild'

FILE_PKGBUILD = DIR_BUILD / 'PKGBUILD'
FILE_SRCINFO = DIR_BUILD / '.SRCINFO'

REQUIRED_TOOLS = ['sha256sum']


def getUrl() -> str:
    return REPOSITORY_URL if REPOSITORY_URL.endswith('/') \
        else f'{REPOSITORY_URL}/'


def getChecksum() -> str:
    return subprocess.check_output(['sha256sum', FILE_INSTALLER]).decode().split()[0]


def getContent() -> str:
    return r'''# Maintainer: '''f'{AUTHOR_NAME}'r''' <'''f'{AUTHOR_EMAIL}'r'''>

_appname="'''f'{NAME_APP_LOWER}'r'''"
pkgname="${_appname}-bin"
pkgver="'''f'{PACKAGE_VERSION}'r'''"
pkgrel=1
pkgdesc="'''f'{PACKAGE_DESCRIPTION}'r'''"
arch=('x86_64')
url="'''f'{getUrl()}'r'''"
license=('MIT')
makedepends=('coreutils' 'tar')
provides=("${_appname}")
options=('!strip')
_install_script="setup_${_appname}_${pkgver}_linux_${arch}.bash"
source=("${url}downloads/${_install_script}")
sha256sums=("'''f'{getChecksum()}'r'''")

package() {
    script="${srcdir}/${_install_script}"
    bash "${script}" -r "${pkgdir}"
}
'''


def setup() -> None:
    if platform.system() != 'Linux':
        print("Pkgbuild can only be generated on Linux!")
        sys.exit(1)

    missingTools = [tool for tool in REQUIRED_TOOLS if not isInPath(tool)]
    if len(missingTools) > 0:
        print(f"Cannot generate pkgbuild! Tools missing from path: {', '.join(missingTools)}")

    print(f"Creating pkgbuild file: {FILE_PKGBUILD}")
    with open(FILE_PKGBUILD, 'w') as fOutput:
        fOutput.write(getContent())

    print(f"Generating srcinfo file: {FILE_SRCINFO}")
    os.system(f'cd "{FILE_PKGBUILD.parent}" ; makepkg --printsrcinfo > {FILE_SRCINFO}')

    print("Finished!")


def clean() -> None:
    filesToRemove = [FILE_PKGBUILD, FILE_SRCINFO]
    for filePath in filesToRemove:
        if filePath.exists():
            print(f"Removing: {filePath}")
            filePath.unlink()


class Target(TargetBase):
    Clean = auto()
    Build = auto()


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(Target.argName(), help=Target.argHelp(), choices=Target.all())
    parsedArgs = parser.parse_args()

    target = Target[getattr(parsedArgs, Target.argName()).capitalize()]
    if target == Target.Build:
        setup()
        return

    clean()


if __name__ == '__main__':
    main()
