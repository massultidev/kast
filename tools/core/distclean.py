#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import shutil
from pathlib import Path

from common import DIR_PACKAGE_ROOT, DIR_ROOT, PACKAGE_NAME

_ARTIFACTS: list[Path] = [
    DIR_ROOT / 'dist',
    DIR_ROOT / 'build',
    DIR_PACKAGE_ROOT / f'{PACKAGE_NAME}.egg-info',
]


def main() -> None:
    print(' -- Perfoming distclean...')
    removeArtifacts()
    print(' -- Distclean done!')


def removeArtifacts() -> None:
    for artifact in _ARTIFACTS:
        removeIfExists(artifact)


def removeIfExists(path: Path) -> None:
    if not path.exists():
        print(f"[-] Attempting to remove: '{path}' - Failed (Does not exist.)")
        return

    shutil.rmtree(path)
    print(f"[+] Attempting to remove: '{path}' - Success")


if __name__ == '__main__':
    main()
