#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import argparse
import platform
import sys
from pathlib import Path

from PyQt5.QtWidgets import QApplication


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('-y', '--noconfirm', help="destructive actions without confirmation", action='store_true')
    parsedArgs = parser.parse_args()

    raiseIf(platform.system() != 'Windows', "This tweak is only for windows!")

    print(
        f"This tweak will try to find and remove Direct Show DLL from your PyQt installation.\n"
        "(You can revert this action only by re-installing pyqt!)\n"
    )

    lPaths = QApplication.libraryPaths()
    pluginsPath = next((p for p in lPaths if p.endswith('plugins')), None)

    raiseIf(not pluginsPath, "Could not find plugins path!")

    mediaServicePath = Path(pluginsPath) / 'mediaservice'
    dsEnginePath = mediaServicePath / 'dsengine.dll'
    wmfEnginePath = mediaServicePath / 'wmfengine.dll'

    if not wmfEnginePath.exists():
        print('WMF engine not found! Codec pack installation might be required!')
        return

    if not dsEnginePath.exists():
        print("DS engine not found. Applying tweak should not be required.")
        return

    print(f"Following file will be removed: '{dsEnginePath}'")
    if not parsedArgs.noconfirm:
        answer = input("Would you like to proceed? [y/N]: ").lower()
        if not answer.startswith('y'):
            sys.exit(1)

    dsEnginePath.unlink()

    print("\nFinished!\n")


def raiseIf(condition: bool, message: str) -> None:
    if condition:
        raise RuntimeError(message)


if __name__ == "__main__":
    main()
