#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from core.common import DIR_CORE, DIR_TOOLS, runScript


def main() -> None:
    runScript(DIR_TOOLS / 'release_linux.py')
    runScript(DIR_CORE / 'linux_create_pkgbuild.py', 'clean')
    runScript(DIR_CORE / 'linux_create_pkgbuild.py', 'build')


if __name__ == '__main__':
    main()
