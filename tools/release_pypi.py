#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import subprocess
import sys

from core.common import DIR_ROOT, DIR_TOOLS, PACKAGE_NAME, PACKAGE_VERSION, isInPath, uninstallProjectPackage


def main() -> None:
    pypiUpdateTool = 'twine'
    if not isInPath(pypiUpdateTool):
        print(f"Could not find PyPI update tool in path! Install: '{pypiUpdateTool}'")
        sys.exit(1)

    uninstallProjectPackage()
    subprocess.run(['python', '-m', 'build', '--sdist'], cwd=DIR_ROOT, check=True)
    subprocess.run([
        pypiUpdateTool,
        'upload',
        '--non-interactive',
        f"--config-file={DIR_TOOLS / 'security' / 'pypi.token'}",
        str(DIR_ROOT / f'dist/{PACKAGE_NAME}-{PACKAGE_VERSION}.tar.gz')
    ], cwd=DIR_ROOT, check=True)


if __name__ == '__main__':
    main()
