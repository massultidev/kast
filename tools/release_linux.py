#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from core.common import DIR_CORE, reinstallProjectPackage, runScript


def main() -> None:
    reinstallProjectPackage()
    runScript(DIR_CORE / 'create_bin_native.py', 'distclean')
    runScript(DIR_CORE / 'create_bin_native.py', 'dir')
    runScript(DIR_CORE / 'linux_create_installer.py', 'clean')
    runScript(DIR_CORE / 'linux_create_installer.py', 'build')
    runScript(DIR_CORE / 'linux_create_installer.py', 'upload')


if __name__ == '__main__':
    main()
