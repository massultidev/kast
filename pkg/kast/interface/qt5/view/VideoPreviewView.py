# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pkg/kast/interface/qt5/view/VideoPreviewView.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_VideoPreviewView(object):
    def setupUi(self, VideoPreviewView):
        VideoPreviewView.setObjectName("VideoPreviewView")
        VideoPreviewView.resize(400, 300)
        VideoPreviewView.setMinimumSize(QtCore.QSize(400, 300))
        VideoPreviewView.setAutoFillBackground(False)
        self._layout = QtWidgets.QVBoxLayout(VideoPreviewView)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(0)
        self._layout.setObjectName("_layout")
        self._widgetPreview = QtWidgets.QWidget(VideoPreviewView)
        self._widgetPreview.setStyleSheet("")
        self._widgetPreview.setObjectName("_widgetPreview")
        self.layoutPreview = QtWidgets.QVBoxLayout(self._widgetPreview)
        self.layoutPreview.setContentsMargins(0, 0, 0, 0)
        self.layoutPreview.setSpacing(0)
        self.layoutPreview.setObjectName("layoutPreview")
        self._layout.addWidget(self._widgetPreview)

        self.retranslateUi(VideoPreviewView)
        QtCore.QMetaObject.connectSlotsByName(VideoPreviewView)

    def retranslateUi(self, VideoPreviewView):
        _translate = QtCore.QCoreApplication.translate
        VideoPreviewView.setWindowTitle(_translate("VideoPreviewView", "Form"))
