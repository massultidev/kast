# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pkg/kast/interface/qt5/view/PreCastConversionView.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PreCastConversionView(object):
    def setupUi(self, PreCastConversionView):
        PreCastConversionView.setObjectName("PreCastConversionView")
        PreCastConversionView.resize(525, 425)
        PreCastConversionView.setModal(True)
        self._layout = QtWidgets.QVBoxLayout(PreCastConversionView)
        self._layout.setObjectName("_layout")
        self._groupBoxInfo = QtWidgets.QGroupBox(PreCastConversionView)
        self._groupBoxInfo.setAutoFillBackground(False)
        self._groupBoxInfo.setTitle("")
        self._groupBoxInfo.setFlat(False)
        self._groupBoxInfo.setCheckable(False)
        self._groupBoxInfo.setObjectName("_groupBoxInfo")
        self.layoutInfoBox = QtWidgets.QHBoxLayout(self._groupBoxInfo)
        self.layoutInfoBox.setObjectName("layoutInfoBox")
        self.labelInfoText = QtWidgets.QLabel(self._groupBoxInfo)
        self.labelInfoText.setObjectName("labelInfoText")
        self.layoutInfoBox.addWidget(self.labelInfoText)
        self._layout.addWidget(self._groupBoxInfo)
        self._deviceModelSelectorLayout = QtWidgets.QHBoxLayout()
        self._deviceModelSelectorLayout.setObjectName("_deviceModelSelectorLayout")
        self._labelDeviceModel = QtWidgets.QLabel(PreCastConversionView)
        self._labelDeviceModel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self._labelDeviceModel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self._labelDeviceModel.setObjectName("_labelDeviceModel")
        self._deviceModelSelectorLayout.addWidget(self._labelDeviceModel)
        self.comboBoxDeviceModel = QtWidgets.QComboBox(PreCastConversionView)
        self.comboBoxDeviceModel.setObjectName("comboBoxDeviceModel")
        self._deviceModelSelectorLayout.addWidget(self.comboBoxDeviceModel)
        self._deviceModelSelectorLayout.setStretch(1, 1)
        self._layout.addLayout(self._deviceModelSelectorLayout)
        self.tableConversion = QtWidgets.QTableWidget(PreCastConversionView)
        self.tableConversion.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableConversion.sizePolicy().hasHeightForWidth())
        self.tableConversion.setSizePolicy(sizePolicy)
        self.tableConversion.setFocusPolicy(QtCore.Qt.NoFocus)
        self.tableConversion.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.tableConversion.setFrameShadow(QtWidgets.QFrame.Plain)
        self.tableConversion.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tableConversion.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableConversion.setTabKeyNavigation(False)
        self.tableConversion.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.tableConversion.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableConversion.setShowGrid(True)
        self.tableConversion.setCornerButtonEnabled(False)
        self.tableConversion.setObjectName("tableConversion")
        self.tableConversion.setColumnCount(4)
        self.tableConversion.setRowCount(4)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableConversion.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.NoBrush)
        item.setBackground(brush)
        self.tableConversion.setItem(2, 0, item)
        self.tableConversion.horizontalHeader().setCascadingSectionResizes(False)
        self.tableConversion.horizontalHeader().setDefaultSectionSize(30)
        self.tableConversion.horizontalHeader().setHighlightSections(False)
        self.tableConversion.horizontalHeader().setMinimumSectionSize(30)
        self.tableConversion.horizontalHeader().setStretchLastSection(True)
        self.tableConversion.verticalHeader().setVisible(True)
        self.tableConversion.verticalHeader().setDefaultSectionSize(30)
        self.tableConversion.verticalHeader().setHighlightSections(False)
        self.tableConversion.verticalHeader().setStretchLastSection(True)
        self._layout.addWidget(self.tableConversion)
        self._layoutSpeedTotal = QtWidgets.QHBoxLayout()
        self._layoutSpeedTotal.setObjectName("_layoutSpeedTotal")
        self._labelSpeedTotal = QtWidgets.QLabel(PreCastConversionView)
        self._labelSpeedTotal.setObjectName("_labelSpeedTotal")
        self._layoutSpeedTotal.addWidget(self._labelSpeedTotal)
        self.lineEditSpeedTotal = QtWidgets.QLineEdit(PreCastConversionView)
        self.lineEditSpeedTotal.setText("")
        self.lineEditSpeedTotal.setReadOnly(True)
        self.lineEditSpeedTotal.setObjectName("lineEditSpeedTotal")
        self._layoutSpeedTotal.addWidget(self.lineEditSpeedTotal)
        self._layout.addLayout(self._layoutSpeedTotal)
        self.buttonBox = QtWidgets.QDialogButtonBox(PreCastConversionView)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self._layout.addWidget(self.buttonBox)

        self.retranslateUi(PreCastConversionView)
        self.buttonBox.accepted.connect(PreCastConversionView.accept) # type: ignore
        self.buttonBox.rejected.connect(PreCastConversionView.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(PreCastConversionView)

    def retranslateUi(self, PreCastConversionView):
        _translate = QtCore.QCoreApplication.translate
        PreCastConversionView.setWindowTitle(_translate("PreCastConversionView", "Media Conversion"))
        self.labelInfoText.setText(_translate("PreCastConversionView", "<html><head/><body><p>Selected media might require conversion before casting.<br/>Please select media support profile for your device model below.<br/>(If you do not know your device model, pick &quot;Chromecast 3&quot;.)</p></body></html>"))
        self._labelDeviceModel.setText(_translate("PreCastConversionView", "Media support profile:"))
        item = self.tableConversion.verticalHeaderItem(0)
        item.setText(_translate("PreCastConversionView", "Container"))
        item = self.tableConversion.verticalHeaderItem(1)
        item.setText(_translate("PreCastConversionView", "Resolution"))
        item = self.tableConversion.verticalHeaderItem(2)
        item.setText(_translate("PreCastConversionView", "Video Codec"))
        item = self.tableConversion.verticalHeaderItem(3)
        item.setText(_translate("PreCastConversionView", "Audio Codec"))
        item = self.tableConversion.horizontalHeaderItem(0)
        item.setText(_translate("PreCastConversionView", "Current"))
        item.setToolTip(_translate("PreCastConversionView", "Current media trait"))
        item = self.tableConversion.horizontalHeaderItem(1)
        item.setText(_translate("PreCastConversionView", "Supported"))
        item.setToolTip(_translate("PreCastConversionView", "Device supported trait options"))
        item = self.tableConversion.horizontalHeaderItem(2)
        item.setText(_translate("PreCastConversionView", "Target"))
        item.setToolTip(_translate("PreCastConversionView", "Target media trait"))
        item = self.tableConversion.horizontalHeaderItem(3)
        item.setText(_translate("PreCastConversionView", "Speed"))
        item.setToolTip(_translate("PreCastConversionView", "Trait conversion speed"))
        __sortingEnabled = self.tableConversion.isSortingEnabled()
        self.tableConversion.setSortingEnabled(False)
        self.tableConversion.setSortingEnabled(__sortingEnabled)
        self._labelSpeedTotal.setText(_translate("PreCastConversionView", "Estimated conversion speed:"))
