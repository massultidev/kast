# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pkg/kast/interface/qt5/view/MediaControlView.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MediaControlView(object):
    def setupUi(self, MediaControlView):
        MediaControlView.setObjectName("MediaControlView")
        MediaControlView.resize(849, 112)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(MediaControlView)
        self.verticalLayout_2.setContentsMargins(0, 6, 0, 6)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.setObjectName("_layout")
        self._layoutMediaControl = QtWidgets.QGridLayout()
        self._layoutMediaControl.setObjectName("_layoutMediaControl")
        self.buttonPlayPause = QtWidgets.QPushButton(MediaControlView)
        self.buttonPlayPause.setText("")
        self.buttonPlayPause.setObjectName("buttonPlayPause")
        self._layoutMediaControl.addWidget(self.buttonPlayPause, 1, 3, 1, 1)
        self.buttonSeekFront = QtWidgets.QPushButton(MediaControlView)
        self.buttonSeekFront.setText("")
        self.buttonSeekFront.setObjectName("buttonSeekFront")
        self._layoutMediaControl.addWidget(self.buttonSeekFront, 1, 5, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self._layoutMediaControl.addItem(spacerItem, 1, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self._layoutMediaControl.addItem(spacerItem1, 1, 6, 1, 1)
        self.buttonSeekBack = QtWidgets.QPushButton(MediaControlView)
        self.buttonSeekBack.setText("")
        self.buttonSeekBack.setObjectName("buttonSeekBack")
        self._layoutMediaControl.addWidget(self.buttonSeekBack, 1, 2, 1, 1)
        self._layoutLeftSideControls = QtWidgets.QGridLayout()
        self._layoutLeftSideControls.setObjectName("_layoutLeftSideControls")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self._layoutLeftSideControls.addItem(spacerItem2, 2, 1, 1, 1)
        self.buttonDisconnect = QtWidgets.QPushButton(MediaControlView)
        self.buttonDisconnect.setObjectName("buttonDisconnect")
        self._layoutLeftSideControls.addWidget(self.buttonDisconnect, 0, 0, 1, 1)
        self.labelTime = QtWidgets.QLabel(MediaControlView)
        self.labelTime.setStyleSheet("font: bold")
        self.labelTime.setObjectName("labelTime")
        self._layoutLeftSideControls.addWidget(self.labelTime, 2, 0, 1, 1)
        self._layoutMediaControl.addLayout(self._layoutLeftSideControls, 1, 0, 1, 1)
        self._layoutRightSideControls = QtWidgets.QGridLayout()
        self._layoutRightSideControls.setHorizontalSpacing(10)
        self._layoutRightSideControls.setVerticalSpacing(0)
        self._layoutRightSideControls.setObjectName("_layoutRightSideControls")
        self.sliderVolumeLocal = QtWidgets.QSlider(MediaControlView)
        self.sliderVolumeLocal.setMaximum(100)
        self.sliderVolumeLocal.setProperty("value", 100)
        self.sliderVolumeLocal.setOrientation(QtCore.Qt.Horizontal)
        self.sliderVolumeLocal.setInvertedAppearance(False)
        self.sliderVolumeLocal.setInvertedControls(False)
        self.sliderVolumeLocal.setTickPosition(QtWidgets.QSlider.TicksAbove)
        self.sliderVolumeLocal.setTickInterval(10)
        self.sliderVolumeLocal.setObjectName("sliderVolumeLocal")
        self._layoutRightSideControls.addWidget(self.sliderVolumeLocal, 1, 2, 1, 1)
        self.sliderVolumeRemote = QtWidgets.QSlider(MediaControlView)
        self.sliderVolumeRemote.setMaximum(100)
        self.sliderVolumeRemote.setProperty("value", 100)
        self.sliderVolumeRemote.setOrientation(QtCore.Qt.Horizontal)
        self.sliderVolumeRemote.setInvertedAppearance(False)
        self.sliderVolumeRemote.setInvertedControls(False)
        self.sliderVolumeRemote.setTickPosition(QtWidgets.QSlider.TicksAbove)
        self.sliderVolumeRemote.setTickInterval(10)
        self.sliderVolumeRemote.setObjectName("sliderVolumeRemote")
        self._layoutRightSideControls.addWidget(self.sliderVolumeRemote, 2, 2, 1, 1)
        self.buttonMuteLocal = QtWidgets.QPushButton(MediaControlView)
        self.buttonMuteLocal.setText("")
        self.buttonMuteLocal.setObjectName("buttonMuteLocal")
        self._layoutRightSideControls.addWidget(self.buttonMuteLocal, 1, 3, 1, 1)
        self.labelVolumeRemote = QtWidgets.QLabel(MediaControlView)
        self.labelVolumeRemote.setObjectName("labelVolumeRemote")
        self._layoutRightSideControls.addWidget(self.labelVolumeRemote, 2, 1, 1, 1)
        self.labelVolumeLocal = QtWidgets.QLabel(MediaControlView)
        self.labelVolumeLocal.setObjectName("labelVolumeLocal")
        self._layoutRightSideControls.addWidget(self.labelVolumeLocal, 1, 1, 1, 1)
        self.buttonMuteRemote = QtWidgets.QPushButton(MediaControlView)
        self.buttonMuteRemote.setText("")
        self.buttonMuteRemote.setObjectName("buttonMuteRemote")
        self._layoutRightSideControls.addWidget(self.buttonMuteRemote, 2, 3, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self._layoutRightSideControls.addItem(spacerItem3, 2, 0, 1, 1)
        self._layoutMediaControl.addLayout(self._layoutRightSideControls, 1, 7, 1, 1)
        self.buttonStop = QtWidgets.QPushButton(MediaControlView)
        self.buttonStop.setText("")
        self.buttonStop.setObjectName("buttonStop")
        self._layoutMediaControl.addWidget(self.buttonStop, 1, 4, 1, 1)
        self._layoutMediaControl.setColumnStretch(0, 1)
        self._layoutMediaControl.setColumnStretch(1, 1)
        self._layoutMediaControl.setColumnStretch(6, 1)
        self._layoutMediaControl.setColumnStretch(7, 1)
        self._layout.addLayout(self._layoutMediaControl)
        self.sliderSeek = QtWidgets.QSlider(MediaControlView)
        self.sliderSeek.setOrientation(QtCore.Qt.Horizontal)
        self.sliderSeek.setObjectName("sliderSeek")
        self._layout.addWidget(self.sliderSeek)
        self.verticalLayout_2.addLayout(self._layout)

        self.retranslateUi(MediaControlView)
        QtCore.QMetaObject.connectSlotsByName(MediaControlView)

    def retranslateUi(self, MediaControlView):
        _translate = QtCore.QCoreApplication.translate
        MediaControlView.setWindowTitle(_translate("MediaControlView", "MediaControlView"))
        self.buttonPlayPause.setToolTip(_translate("MediaControlView", "Play/Pause"))
        self.buttonSeekFront.setToolTip(_translate("MediaControlView", "Seek front"))
        self.buttonSeekBack.setToolTip(_translate("MediaControlView", "Seek back"))
        self.buttonDisconnect.setText(_translate("MediaControlView", "Disconnect"))
        self.labelTime.setText(_translate("MediaControlView", "00:00 / 00:00"))
        self.sliderVolumeLocal.setToolTip(_translate("MediaControlView", "Preview volume level"))
        self.sliderVolumeRemote.setToolTip(_translate("MediaControlView", "Remote volume level"))
        self.buttonMuteLocal.setToolTip(_translate("MediaControlView", "Preview mute"))
        self.labelVolumeRemote.setText(_translate("MediaControlView", "Remote:"))
        self.labelVolumeLocal.setText(_translate("MediaControlView", "Preview:"))
        self.buttonMuteRemote.setToolTip(_translate("MediaControlView", "Remote mute"))
        self.buttonStop.setToolTip(_translate("MediaControlView", "Stop"))
        self.sliderSeek.setToolTip(_translate("MediaControlView", "Video position"))
