#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from abc import ABC, abstractmethod
from collections.abc import Callable
from functools import wraps
from typing import Any, Concatenate


class NonSchedulableException(Exception):
    pass


class ISchedulable(ABC):
    @staticmethod
    @abstractmethod
    def _verifySchedulable(obj: Any) -> None: ...


type SchedulableCallback[**P, S: ISchedulable] = Callable[Concatenate[S, P], None]
type DecoratorProviderReturn[**P] = Callable[[Callable[P, None]], Callable[P, None]]
type DecoratorProvider[**P, S: ISchedulable] = Callable[[S], DecoratorProviderReturn[P]]


class AbstractSchedulable(ISchedulable, ABC):

    @classmethod
    def _taskDecorator[**P, S: ISchedulable](
        cls,
        funcOpt: SchedulableCallback[P, S] | None,
        *,
        decoratorProvider: DecoratorProvider
    ) -> Callable[[SchedulableCallback[P, S]], SchedulableCallback[P, S]] | SchedulableCallback[P, S]:
        def decorator(func: SchedulableCallback[P, S]) -> SchedulableCallback[P, S]:
            @wraps(func)
            def wrapper(schedulable: S, *args: P.args, **kwargs: P.kwargs) -> None:
                cls._verifySchedulable(schedulable)

                @decoratorProvider(schedulable)
                def callback() -> None:
                    func(schedulable, *args, **kwargs)

                callback()

            return wrapper

        decoratedFunction = decorator if funcOpt is None \
            else decorator(funcOpt)
        return decoratedFunction
