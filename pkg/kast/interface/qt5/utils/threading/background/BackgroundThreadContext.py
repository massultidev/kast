#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from collections.abc import Callable
from functools import wraps
from typing import overload

from kast.interface.qt5.utils.QtHelper import QtHelper
from kast.utils.functional.callable import ErrorHandler
from kast.utils.threading.Scheduler import Scheduler

type BackgroundTask[**P] = Callable[P, None]


class BackgroundThreadContext:

    def __init__(self, backgroundScheduler: Scheduler) -> None:
        self._backgroundScheduler: Scheduler = backgroundScheduler

    @overload
    def backgroundTask[**P](
        self,
        funcOpt: BackgroundTask[P],
        /
    ) -> BackgroundTask[P]: ...

    @overload
    def backgroundTask[**P](
        self,
        *,
        forceSchedule: bool,
        errorHandler: ErrorHandler | None
    ) -> Callable[[BackgroundTask[P]], BackgroundTask[P]]: ...

    def backgroundTask[**P](
        self,
        funcOpt: BackgroundTask[P] | None = None,
        *,
        forceSchedule: bool = False,
        errorHandler: ErrorHandler | None = None
    ) -> Callable[[BackgroundTask[P]], BackgroundTask[P]] | BackgroundTask[P]:
        def decorator(func: BackgroundTask[P]) -> BackgroundTask[P]:
            @wraps(func)
            def wrapper(*args: P.args, **kwargs: P.kwargs) -> None:
                def callback() -> None:
                    try:
                        func(*args, **kwargs)

                    except Exception as ex:
                        if errorHandler is None:
                            raise ex
                        errorHandler(ex)

                if not forceSchedule and not QtHelper.isMainThread():
                    callback()
                    return

                self._backgroundScheduler.schedule(callback)

            return wrapper

        decoratedFunction = decorator if funcOpt is None \
            else decorator(funcOpt)
        return decoratedFunction
