#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from collections.abc import Callable
from typing import Any, Concatenate, overload

from kast.interface.qt5.utils.threading.AbstractSchedulable import AbstractSchedulable, DecoratorProviderReturn, \
    ISchedulable, \
    NonSchedulableException, SchedulableCallback
from kast.interface.qt5.utils.threading.background.BackgroundThreadContext import BackgroundTask, \
    BackgroundThreadContext


class BackgroundSchedulable(AbstractSchedulable):

    def __init__(self, backgroundThreadContext: BackgroundThreadContext) -> None:
        self.__backgroundThreadContext: BackgroundThreadContext = backgroundThreadContext

    def _onBackgroundException(self, ex: Exception) -> None:
        raise ex

    @classmethod
    @overload
    def backgroundTask[**P, S: ISchedulable](
        cls,
        funcOpt: SchedulableCallback[P, S],
        /
    ) -> SchedulableCallback[P, S]: ...

    @classmethod
    @overload
    def backgroundTask[**P, S: ISchedulable](
        cls,
        *,
        forceSchedule: bool
    ) -> Callable[[SchedulableCallback[P, S]], SchedulableCallback[P, S]]: ...

    @classmethod
    def backgroundTask[**P, S: ISchedulable](
        cls,
        funcOpt: SchedulableCallback[P, S] | None = None,
        *,
        forceSchedule: bool = False
    ) -> Callable[[SchedulableCallback[P, S]], SchedulableCallback[P, S]] | SchedulableCallback[P, S]:
        def decoratorProvider(schedulable: BackgroundSchedulable) -> DecoratorProviderReturn[P]:
            return schedulable.__backgroundThreadContext.backgroundTask(
                forceSchedule=forceSchedule,
                errorHandler=schedulable._onBackgroundException
            )

        return cls._taskDecorator(funcOpt=funcOpt, decoratorProvider=decoratorProvider)

    @staticmethod
    def _verifySchedulable(obj: Any) -> None:
        if not isinstance(obj, BackgroundSchedulable):
            raise NonSchedulableException(f"Object of type '{obj.__class__.__name__}' does not derive from '{BackgroundSchedulable.__name__}'!")
