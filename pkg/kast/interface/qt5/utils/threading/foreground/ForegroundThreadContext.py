#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from collections.abc import Callable
from functools import wraps
from typing import overload

from kast.interface.qt5.utils.QtHelper import QtHelper
from kast.interface.qt5.utils.threading.foreground.ForegroundScheduler import ForegroundScheduler

type ForegroundTask[**P] = Callable[P, None]


class ForegroundThreadContext:

    def __init__(self, foregroundScheduler: ForegroundScheduler) -> None:
        self._foregroundScheduler: ForegroundScheduler = foregroundScheduler

    @overload
    def foregroundTask[**P](
        self,
        funcOpt: ForegroundTask[P],
        /
    ) -> ForegroundTask[P]: ...

    @overload
    def foregroundTask[**P](
        self,
        *,
        forceSchedule: bool
    ) -> Callable[[ForegroundTask[P]], ForegroundTask[P]]: ...

    def foregroundTask[**P](
        self,
        funcOpt: ForegroundTask[P] | None = None,
        *,
        forceSchedule: bool = False,
    ) -> Callable[[ForegroundTask[P]], ForegroundTask[P]] | ForegroundTask[P]:
        def decorator(func: ForegroundTask[P]) -> ForegroundTask[P]:
            @wraps(func)
            def wrapper(*args: P.args, **kwargs: P.kwargs) -> None:
                def callback() -> None:
                    func(*args, **kwargs)

                if not forceSchedule and QtHelper.isMainThread():
                    callback()
                    return

                self._foregroundScheduler.schedule(callback)

            return wrapper

        decoratedFunction = decorator if funcOpt is None \
            else decorator(funcOpt)
        return decoratedFunction
