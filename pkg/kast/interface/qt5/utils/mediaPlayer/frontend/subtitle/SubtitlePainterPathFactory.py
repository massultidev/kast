#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from enum import Enum, auto

from PyQt5.QtCore import QRect
from PyQt5.QtGui import QFont, QFontMetrics, QPainterPath

from kast.interface.qt5.utils.mediaPlayer.frontend.subtitle.SubtitlePaintTokenizer import SubtitlePaintTokenizer


class HorizontalOrientation(Enum):
    Center = auto()
    Left = auto()
    Right = auto()


class VerticalOrientation(Enum):
    Center = auto()
    Top = auto()
    Bottom = auto()


class SubtitlePainterPathFactory:

    def __init__(self, font: QFont) -> None:
        self._font: QFont = font
        self._fontMetrics: QFontMetrics = QFontMetrics(font)

        self._subtitlePaintTokenizer: SubtitlePaintTokenizer = SubtitlePaintTokenizer(font=font)

    def create(
        self,
        text: str,
        boundingRect: QRect,
        xOrientation: HorizontalOrientation = HorizontalOrientation.Left,
        yOrientation: VerticalOrientation = VerticalOrientation.Top
    ) -> QPainterPath:
        painterPath = QPainterPath()

        posX = boundingRect.x()
        posY = boundingRect.y()
        lines = list(self._subtitlePaintTokenizer.tokenize(text=text, maxPixelWidth=boundingRect.width()))
        for line in lines:
            posY += self._fontMetrics.lineSpacing()
            if (posY + self._fontMetrics.descent()) > boundingRect.bottom():
                break

            offsetX = self._getOffsetX(
                line=line,
                maxPixelWidth=boundingRect.width(),
                xOrientation=xOrientation
            )
            offsetY = self._getOffsetY(
                numberOfLines=len(lines),
                maxPixelHeight=boundingRect.height(),
                yOrientation=yOrientation
            )

            painterPath.addText(posX + offsetX, posY + offsetY, self._font, line)

        return painterPath

    def _getOffsetX(
        self,
        line: str,
        maxPixelWidth: int,
        xOrientation: HorizontalOrientation
    ) -> int:
        xDelta = maxPixelWidth - self._fontMetrics.horizontalAdvance(line)
        if xOrientation is HorizontalOrientation.Right:
            return xDelta

        if xOrientation is HorizontalOrientation.Center:
            return int(xDelta / 2)

        return 0

    def _getOffsetY(
        self,
        numberOfLines: int,
        maxPixelHeight: int,
        yOrientation: VerticalOrientation
    ) -> int:
        yDelta = maxPixelHeight - (numberOfLines * self._fontMetrics.lineSpacing())
        if yDelta < 0:
            return 0

        if yOrientation is VerticalOrientation.Bottom:
            return yDelta - self._fontMetrics.descent()

        if yOrientation is VerticalOrientation.Center:
            return int((yDelta / 2) - self._fontMetrics.descent())

        return 0
