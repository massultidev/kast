#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from collections.abc import Iterator
from dataclasses import dataclass

from PyQt5.QtGui import QFont, QFontMetrics


@dataclass(frozen=True)
class LineSplitInfo:
    splitBeforeIndex: int
    splitAfterIndex: int
    conjunctiveChar: str = ''


class SubtitlePaintTokenizer:

    _WORD_BREAK_CONJUNCTION: str = '-'

    def __init__(self, font: QFont) -> None:
        self._fontMetrics: QFontMetrics = QFontMetrics(font)

    def tokenize(
        self,
        text: str,
        maxPixelWidth: int
    ) -> Iterator[str]:
        for line in text.split('\n'):
            for token in self._makeLineTokenizer(
                line=line,
                maxPixelWidth=maxPixelWidth
            ):
                yield token

    def _makeLineTokenizer(
        self,
        line: str,
        maxPixelWidth: int
    ) -> Iterator[str]:
        lineLength = len(line)
        if lineLength <= 0:
            yield line

        while len(line) > 0:
            splitLineInfo = self._findLineSplit(
                line=line,
                maxPixelWidth=maxPixelWidth
            )

            token, line = (
                line[:splitLineInfo.splitBeforeIndex] + splitLineInfo.conjunctiveChar,
                line[splitLineInfo.splitAfterIndex:]
            )

            yield token

    def _findLineSplit(
        self,
        line: str,
        maxPixelWidth: int
    ) -> LineSplitInfo:
        def doesLineChunkFit(charCount: int) -> bool:
            return self._fontMetrics.horizontalAdvance(line, charCount) <= maxPixelWidth

        lineLength = len(line)
        if doesLineChunkFit(charCount=lineLength):
            return LineSplitInfo(
                splitBeforeIndex=lineLength,
                splitAfterIndex=lineLength,
            )

        for wordSplitIndex in self._makeWhiteSpaceIndexProvider(line=line):
            if doesLineChunkFit(charCount=wordSplitIndex):
                return LineSplitInfo(
                    splitBeforeIndex=wordSplitIndex,
                    splitAfterIndex=wordSplitIndex + 1,
                )

        for charSplitIndex in range(len(line), 0, -1):
            if doesLineChunkFit(charCount=charSplitIndex):
                return LineSplitInfo(
                    splitBeforeIndex=charSplitIndex,
                    splitAfterIndex=charSplitIndex,
                    conjunctiveChar=self._WORD_BREAK_CONJUNCTION,
                )

        return LineSplitInfo(
            splitBeforeIndex=lineLength,
            splitAfterIndex=lineLength,
        )

    @staticmethod
    def _makeWhiteSpaceIndexProvider(line: str) -> Iterator[int]:
        endIndex = len(line)
        while endIndex > 0:
            endIndex = line.rfind(' ', 0, endIndex)
            yield endIndex
