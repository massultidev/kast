#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from enum import Enum


class SubtitleStyle(Enum):
    Shadow = 'shadow'
    Outline = 'outline'
