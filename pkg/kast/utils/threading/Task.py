#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from dataclasses import dataclass, field

from tunit.unit import Milliseconds

from kast.utils.functional.callable import Runnable


@dataclass
class Task:
    runnable: Runnable
    scheduledTimestamp: Milliseconds = field(default_factory=lambda: Milliseconds())
