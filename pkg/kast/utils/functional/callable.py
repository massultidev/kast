#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from collections.abc import Callable

type ErrorHandler = Callable[[Exception], None]
type Mapper[T, R] = Callable[[T], R]
type Predicate[T] = Callable[[T], bool]
type Consumer[T] = Callable[[T], None]
type Supplier[T] = Callable[[], T]
type Runnable = Callable[[], None]
