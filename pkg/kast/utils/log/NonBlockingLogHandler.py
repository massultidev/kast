#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import atexit
from logging import Handler
from logging.handlers import QueueHandler, QueueListener
from queue import Queue

_DEFAULT_RESPECT_HANDLER_LEVEL: bool = False


class NonBlockingLogHandler(QueueHandler):
    def __init__(
        self,
        *handlers: Handler,
        respect_handler_level: bool = _DEFAULT_RESPECT_HANDLER_LEVEL
    ) -> None:
        super().__init__(Queue())

        self._listener = QueueListener(self.queue, *handlers, respect_handler_level=respect_handler_level)
        self._listener.start()
        atexit.register(self._listener.stop)


class DictCfgNonBlockingLogHandler(NonBlockingLogHandler):
    def __init__(
        self,
        all_handlers: dict[str, Handler],
        handlers: list[str],
        respect_handler_level: bool = _DEFAULT_RESPECT_HANDLER_LEVEL
    ) -> None:
        super().__init__(
            *[all_handlers[handlerName] for handlerName in handlers],
            respect_handler_level=respect_handler_level
        )
