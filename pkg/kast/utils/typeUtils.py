#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import cast


def castNotNull[T](value: T | None) -> T:
    return cast(T, value)
